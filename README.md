Protocol (from `io-api`) implementation on ktor

```kotlin
import casperix.io.codec.StringCodec
import casperix.io.tcp.TcpBuilder
import io.ktor.network.sockets.*
import kotlin.concurrent.thread

val address = InetSocketAddress("localhost", 8000)

fun main() {
    thread {
        createClient()
    }

    createServer()
}

fun createClient() {
    TcpBuilder.createClient(address, StringCodec, { connection ->
        val input = connection.receiving()
        println("[client] received message: \"$input\"")

        val output = "I know"
        connection.send(output)
        println("[client] message sent: \"$output\"")
    })
}

fun createServer() {
    TcpBuilder.createServer(address, StringCodec, {

    }, { id, connection ->
        val output = "I'm a server"
        connection.send(output)
        println("[server] message sent: \"$output\"")

        val input = connection.receiving()
        println("[server] received message: \"$input\"")
    })
}
```
			