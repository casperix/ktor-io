package casperix.io.codec

import casperix.io.api.ChannelCodec
import casperix.io.codec.core.CustomMessageCodec
import casperix.io.codec.core.LoginMessage
import casperix.io.codec.core.ChatMessage
import io.ktor.utils.io.*
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withTimeout
import kotlin.random.Random
import kotlin.test.Test
import kotlin.test.assertEquals

class CodecTest {
	val random = Random(0)
	val testData = random.nextBytes(10_000_000)

	@Test
	fun string() {
		val allValidChars = (0 until 0xFFFF).map {
			Char(it)
		}.filter {
			//surrogate pairs
			(it.code < 0xD800 || it.code > 0xDFFF)
		}

		val randomValues = allValidChars.map { it.toString() }
		testWithCodec(StringCodec, randomValues)
	}


	@Test
	fun custom() {
		testWithCodec(
			CustomMessageCodec, listOf(
				LoginMessage(Long.MIN_VALUE),
				ChatMessage(Long.MAX_VALUE, "hello server"),
				ChatMessage(Long.MIN_VALUE, "hello client"),
				LoginMessage(1),
				ChatMessage(Long.MAX_VALUE, "again"),
			)
		)
	}

	@Test
	fun binary() {
		testWithCodec(
			BinaryMessageCodec, listOf(
				BinaryMessage(1, byteArrayOf(0, 1, 2)),
				BinaryMessage(4, testData.copyOfRange(0, 7_500_000)),
				BinaryMessage(Int.MAX_VALUE, byteArrayOf(0, 1, 127)),
				BinaryMessage(Int.MIN_VALUE, byteArrayOf(0, 1, -128)),
				BinaryMessage(5, testData.copyOfRange(2_500_000, 10_000_000)),
				BinaryMessage(Int.MAX_VALUE, byteArrayOf(0, 1, 127)),
			)
		)
	}

	private fun <Message> testWithCodec(codec: ChannelCodec<Message>, messages: List<Message>) {
		runBlocking {
			withTimeout(1000L) {
				val channel = ByteChannel(true)

				launch {
					messages.forEachIndexed { index, expected ->
						codec.encode(expected, channel)
					}
				}

				launch {
					messages.forEachIndexed { index, expected ->
						val actual = codec.decode(channel)
						assertEquals(expected, actual)
					}
				}
			}
		}
	}

}