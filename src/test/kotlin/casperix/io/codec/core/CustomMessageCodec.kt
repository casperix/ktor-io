package casperix.io.codec.core

import casperix.io.api.ChannelCodec
import casperix.io.codec.BinaryMessageCodec
import io.ktor.utils.io.*

object CustomMessageCodec : ChannelCodec<CustomMessage> {

	override suspend fun decode(channel: ByteReadChannel): CustomMessage {
		return CustomMessageParser.parser(BinaryMessageCodec.decode(channel))
	}

	override suspend fun encode(message: CustomMessage, channel: ByteWriteChannel) {
		BinaryMessageCodec.encode(CustomMessageParser.parser(message), channel)
	}

}