package casperix.io.codec.core

import kotlinx.serialization.Serializable

sealed class CustomMessage

@Serializable
data class ChatMessage(val sender: Long, val text: String) : CustomMessage()

@Serializable
data class LoginMessage(val clientId: Long) : CustomMessage()

