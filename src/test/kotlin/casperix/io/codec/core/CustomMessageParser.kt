package casperix.io.codec.core

import casperix.io.codec.BinaryMessage
import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.decodeFromByteArray
import kotlinx.serialization.encodeToByteArray
import kotlinx.serialization.protobuf.ProtoBuf

@OptIn(ExperimentalSerializationApi::class)
object CustomMessageParser {
	val PROTOBUF = ProtoBuf{}

	fun parser(source: CustomMessage): BinaryMessage {
		return when (source) {
			is LoginMessage -> BinaryMessage(0, PROTOBUF.encodeToByteArray(source))
			is ChatMessage -> BinaryMessage(1, PROTOBUF.encodeToByteArray(source))
		}
	}

	fun parser(source: BinaryMessage): CustomMessage {
		return when (source.type) {
			0 -> PROTOBUF.decodeFromByteArray<LoginMessage>(source.data)
			1 -> PROTOBUF.decodeFromByteArray<ChatMessage>(source.data)
			else -> throw Error("Invalid message type ${source.type} ")
		}
	}

}