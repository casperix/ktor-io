package casperix.io.codec.core

import casperix.io.api.ChannelCodec
import io.ktor.utils.io.*
import kotlinx.coroutines.channels.ClosedReceiveChannelException

object BrokenStringCodec : ChannelCodec<String> {
	val decodeFailureMessage = "decodeFail"
	val encodeFailureMessage = "encodeFail"

	val encodeFailure = Error(encodeFailureMessage)
	val decodeFailure = Error(decodeFailureMessage)

	override suspend fun decode(channel: ByteReadChannel): String {
		val result = channel.readUTF8Line() ?: throw ClosedReceiveChannelException("channel closed")
		if (result == decodeFailureMessage) {
			throw decodeFailure
		}
		return result
	}

	override suspend fun encode(message: String, channel: ByteWriteChannel) {
		if (message == encodeFailureMessage) {
			throw encodeFailure
		}
		channel.writeStringUtf8(message + "\n")
	}

}