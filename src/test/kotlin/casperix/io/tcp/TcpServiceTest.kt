package casperix.io.tcp

import casperix.io.codec.StringCodec
import casperix.io.tcp.stage.core.TcpTestSupport.createEmptyClient
import casperix.io.tcp.stage.core.TcpTestSupport.createEmptyServer
import casperix.io.tcp.stage.core.TcpTestSupport.lockAddress
import casperix.io.tcp.stage.core.TcpTestSupport.waitTime
import casperix.io.tcp.stage.core.TcpTestSupport.waitUntil
import io.ktor.network.sockets.*
import java.net.BindException
import java.net.ConnectException
import java.util.concurrent.atomic.AtomicInteger
import java.util.concurrent.atomic.AtomicReference
import kotlin.concurrent.thread
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith
import kotlin.test.assertTrue

class TcpServiceTest {

	@Test
	fun createClientWithoutServer() {
		assertFailsWith(ConnectException::class) {
			waitTime {
				createEmptyClient()
			}
		}
	}

	@Test
	fun createAndClose() {
		val socketContainer = AtomicReference<ServerSocket>()

		waitTime {
			TcpBuilder.createServer(lockAddress(), StringCodec, { socket ->
				socketContainer.set(socket)
				socket.dispose()
			}, { id, channel ->

			})
		}

		assertTrue(socketContainer.get() != null)
		assertTrue(socketContainer.get()?.isClosed == true)
	}

	@Test
	fun createTwoServersForOneAddress() {
		val address = lockAddress()

		waitTime {
			thread {
				createEmptyServer(address)
			}

			Thread.sleep(100)

			assertFailsWith(BindException::class) {
				createEmptyServer(address)
			}
		}
	}

	@Test
	fun createBigServerCollection() {
		val maxTimeOut = 2000L
		val serverAmount = 2000
		val controlAmount = AtomicInteger()

		(0 until serverAmount).forEach {
			thread {
				TcpBuilder.createServer(lockAddress(), StringCodec, { info ->
					controlAmount.incrementAndGet()
				}, { id, channel ->

				})
			}
		}

		waitUntil(maxTimeOut, {
			serverAmount == controlAmount.get()
		})

		assertEquals(serverAmount, controlAmount.get(), "Not all servers created")
	}

}
