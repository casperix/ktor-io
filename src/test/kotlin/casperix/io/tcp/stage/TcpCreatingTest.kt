package casperix.io.tcp.stage

import casperix.io.codec.StringCodec
import casperix.io.tcp.stage.core.TcpStageLauncher
import casperix.io.tcp.stage.core.TcpStageCondition.isClientsWorking
import casperix.io.tcp.stage.core.TcpStageCondition.isServerWorking
import casperix.io.tcp.stage.core.TcpStageCondition.noServerErrors
import casperix.io.tcp.stage.core.TcpStageCondition.serverChannelAmount
import casperix.io.tcp.stage.core.TcpTestSupport.lockAddress
import kotlin.test.Test

class TcpCreatingTest {

	@Test
	fun serverAndClient() {
		TcpStageLauncher(
			1, StringCodec, lockAddress(),
			waitingReasonList = listOf(
				{ serverChannelAmount(it, 1) },
				{ isClientsWorking(it, 1) },
				::isServerWorking,
				::noServerErrors,
			)
		)
	}

	@Test
	fun serverWithoutClient() {
		TcpStageLauncher(
			0, StringCodec, lockAddress(),
			waitingReasonList = listOf(
				{ serverChannelAmount(it, 0) },
				{ isClientsWorking(it, 0) },
				::isServerWorking,
				::noServerErrors,
			)
		)
	}

}

