package casperix.io.tcp.stage

import casperix.io.codec.StringCodec
import casperix.io.codec.core.BrokenStringCodec
import casperix.io.tcp.stage.core.TcpChannelEntry
import casperix.io.tcp.stage.core.TcpStageCondition.findClientWithErrors
import casperix.io.tcp.stage.core.TcpStageCondition.isClientsWorking
import casperix.io.tcp.stage.core.TcpStageCondition.isServerWorking
import casperix.io.tcp.stage.core.TcpStageCondition.serverHasErrors
import casperix.io.tcp.stage.core.TcpStageLauncher
import casperix.io.tcp.stage.core.TcpTestSupport.lockAddress
import kotlin.test.Test

class TcpExceptionTest {
	private val maxClientAmount = 3

	@Test
	fun serverSetupChannel() {
		val testError: Throwable = Error("It's test error")

		TcpStageLauncher(maxClientAmount, StringCodec, lockAddress(), { }, {
			if (it.id == 1L) {
				throw testError
			}
		}, { },
			waitingReasonList = listOf(
				{ serverHasErrors(it, listOf(testError)) },
				{ isClientsWorking(it, maxClientAmount - 1) },
				::isServerWorking,
			)
		)
	}

	@Test
	fun clientSetupChannel() {
		val testError: Throwable = Error("It's test error")

		TcpStageLauncher(maxClientAmount, StringCodec, lockAddress(), { }, { }, {
			if (it.id == 1L) {
				throw testError
			}
		},
			waitingReasonList = listOf(
				{ findClientWithErrors(it, listOf(testError)) },
				{ isClientsWorking(it, maxClientAmount - 1) },
				::isServerWorking,
			)
		)
	}

	@Test
	fun clientEncoding() {
		TcpStageLauncher(maxClientAmount, BrokenStringCodec, lockAddress(), {

		}, {

		}, {
			sendValidAndEncodeFail(it)
		},
			waitingReasonList = listOf(
				{ findClientWithErrors(it, listOf(BrokenStringCodec.encodeFailure)) },
				{ isClientsWorking(it, maxClientAmount - 1) },
				::isServerWorking,
			)
		)
	}

	@Test
	fun serverEncoding() {
		TcpStageLauncher(maxClientAmount, BrokenStringCodec, lockAddress(), { }, {
			sendValidAndEncodeFail(it)
		}, {},
			waitingReasonList = listOf(
				{ serverHasErrors(it, listOf(BrokenStringCodec.encodeFailure)) },
				{ isClientsWorking(it, maxClientAmount - 1) },
				::isServerWorking,
			)
		)
	}


	@Test
	fun clientDecoding() {
		TcpStageLauncher(maxClientAmount, BrokenStringCodec, lockAddress(), { }, {}, {
			sendValidAndDecodeFail(it)
		},
			waitingReasonList = listOf(
				{ serverHasErrors(it, listOf(BrokenStringCodec.decodeFailure)) },
				{ isClientsWorking(it, maxClientAmount - 1) },
				::isServerWorking,
			)
		)
	}

	@Test
	fun serverDecoding() {
		TcpStageLauncher(maxClientAmount, BrokenStringCodec, lockAddress(), { }, {
			sendValidAndDecodeFail(it)
		}, {

		},
			waitingReasonList = listOf(
				{ findClientWithErrors(it, listOf(BrokenStringCodec.decodeFailure)) },
				{ isClientsWorking(it, maxClientAmount - 1) },
				::isServerWorking,
			)
		)
	}

	private fun sendValidAndEncodeFail(it: TcpChannelEntry<String>) {
		if (it.id == 1L) {
			it.channel.send(BrokenStringCodec.encodeFailureMessage)
		} else {
			it.channel.send("valid message")
		}
	}

	private fun sendValidAndDecodeFail(it: TcpChannelEntry<String>) {
		if (it.id == 1L) {
			it.channel.send(BrokenStringCodec.decodeFailureMessage)
		} else {
			it.channel.send("valid message")
		}
	}

}

