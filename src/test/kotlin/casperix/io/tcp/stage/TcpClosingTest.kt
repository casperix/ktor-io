package casperix.io.tcp.stage

import casperix.io.codec.StringCodec
import casperix.io.tcp.stage.core.TcpChannelEntry
import casperix.io.tcp.stage.core.TcpStageLauncher
import casperix.io.tcp.stage.core.TcpStageCondition.isClientsWorking
import casperix.io.tcp.stage.core.TcpStageCondition.isServerClosed
import casperix.io.tcp.stage.core.TcpStageCondition.isServerWorking
import casperix.io.tcp.stage.core.TcpStageCondition.noClientErrors
import casperix.io.tcp.stage.core.TcpStageCondition.noServerErrors
import casperix.io.tcp.stage.core.TcpTestSupport.lockAddress
import casperix.io.tcp.stage.core.TcpTestSupport.receiveOrBlock
import io.ktor.network.sockets.*
import java.util.concurrent.atomic.AtomicReference
import kotlin.test.Test
import kotlin.test.assertEquals

class TcpClosingTest {
	private val maxClientAmount = 12

	@Test
	fun closeServerSocket() {
		val serverSocket = AtomicReference<ServerSocket>()

		TcpStageLauncher(maxClientAmount, StringCodec, lockAddress(), {
			serverSocket.set(it)
		}, {
			exchangeMessages(it, true)

		}, {
			exchangeMessages(it, false)
			if (it.id == 1L) {
				serverSocket.get().dispose()
			}
		}, waitingReasonList = listOf(
			{ isClientsWorking(it, 0) },
			::isServerClosed,
			::noServerErrors,
		)
		)
	}

	@Test
	fun closeChannelServerSocket() {
		TcpStageLauncher(maxClientAmount, StringCodec, lockAddress(), {

		}, {
			exchangeMessages(it, true)
			if (it.id == 1L) {
				it.channel.socket.close()
			}
		}, {
			exchangeMessages(it, false)
		}, waitingReasonList = listOf(
			{ isClientsWorking(it, maxClientAmount - 1) },
			::isServerWorking,
			::noServerErrors,
			::noClientErrors,
		)
		)
	}

	@Test
	fun closeChannelClientSocket() {
		val maxClientAmount = 12

		TcpStageLauncher(maxClientAmount, StringCodec, lockAddress(), {

		}, {
			exchangeMessages(it, true)
		}, {
			exchangeMessages(it, false)

			if (it.id == 1L) {
				it.channel.socket.close()
			}

		}, waitingReasonList = listOf(
			{ isClientsWorking(it, maxClientAmount - 1) },
			::isServerWorking,
			::noServerErrors,
		)
		)
	}

	private suspend fun exchangeMessages(connection: TcpChannelEntry<String>, isServer:Boolean) {
		val serverMessage = "hello from server"
		val clientMessage = "hello from client"

		val sendMessage = if (isServer) serverMessage else clientMessage
		val receiveMessage = if (isServer) clientMessage else serverMessage
		connection.channel.send(sendMessage)
		val actualMessage = receiveOrBlock(connection.channel)
		assertEquals(receiveMessage, actualMessage)
	}
}