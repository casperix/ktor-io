package casperix.io.tcp.stage

import casperix.io.codec.StringCodec
import casperix.io.tcp.stage.core.TcpStageCondition
import casperix.io.tcp.stage.core.TcpStageLauncher
import casperix.io.tcp.stage.core.TcpTestSupport
import kotlinx.coroutines.runBlocking
import org.junit.Test
import java.util.concurrent.atomic.AtomicInteger
import kotlin.concurrent.thread
import kotlin.random.Random
import kotlin.time.DurationUnit
import kotlin.time.TimeSource

class TcpPerformanceTest {
    private val alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
    private val random = Random(1)

    private val randomList = (1..100).map { generateRandomString(it * it) }
    private val sourceMessageList = (1..100_000).map { randomList[it % randomList.size] }.shuffled()

    private val messagesTestClientAmount = 2
    private val messagesTestMessageAmount = 100_000

    private val clientsTestClientAmount = 500
    private val clientsTestMessagesAmount = 5

    private val maxTimeMs = 30_000L


    @Test
    fun clientsTest() {
        val messageList = sourceMessageList.subList(0, clientsTestMessagesAmount)

        val receivedAmount = AtomicInteger(0)
        val timeMark = TimeSource.Monotonic.markNow()

        TcpStageLauncher(clientsTestClientAmount, StringCodec, TcpTestSupport.lockAddress(), {

        }, { entry ->
            messageList.forEach {
                entry.channel.sending(it)
            }
        }, { entry ->
            messageList.forEach {
                entry.channel.receiving()
                receivedAmount.incrementAndGet()
            }
        }, waitingReasonList = listOf(
            {
                val actualAmount = receivedAmount.get()
                val expectedAmount = messageList.size * clientsTestClientAmount
                if (actualAmount == expectedAmount) null else "Need $expectedAmount but actual is $actualAmount"
            },
            TcpStageCondition::isServerWorking
        ), timeoutMs = maxTimeMs
        )

        val usedTime = timeMark.elapsedNow()
        assert(usedTime.toLong(DurationUnit.MILLISECONDS) < maxTimeMs) { "Transfer time must be less than $maxTimeMs ms" }
        println("Sent for ${usedTime.toLong(DurationUnit.MILLISECONDS)} ms")
    }

    @Test
    fun messagesTest() {
        val messageList = sourceMessageList.subList(0, messagesTestMessageAmount)
        val receivedAmount = AtomicInteger(0)
        val timeMark = TimeSource.Monotonic.markNow()

        TcpStageLauncher(messagesTestClientAmount, StringCodec, TcpTestSupport.lockAddress(), {

        }, { entry ->
            messageList.forEach {
                entry.channel.send(it)
            }
        }, { entry ->
            messageList.forEach {
                entry.channel.receiving()
                receivedAmount.incrementAndGet()
            }
        }, waitingReasonList = listOf {
            val actualAmount = receivedAmount.get()
            val expectedAmount = messageList.size * messagesTestClientAmount
            if (actualAmount == expectedAmount) null else "Received only $actualAmount but expected $expectedAmount"
        }, timeoutMs = maxTimeMs)


        val summarySizeMb = (messageList.map { it.length }.reduce { acc, i -> acc + i }) / 1024 / 1024
        val usedTime = timeMark.elapsedNow()
        assert(usedTime.toLong(DurationUnit.MILLISECONDS) < maxTimeMs) { "Transfer time of $summarySizeMb Mb must be less than $maxTimeMs ms" }
        println("Sent $summarySizeMb Mb for ${usedTime.toLong(DurationUnit.MILLISECONDS)} ms")
    }

    private fun generateRandomString(length: Int): String {
        val shuffledAlphabet = alphabet.map { it }.shuffled(random).joinToString()

        val builder = StringBuilder()
        while (builder.length < length) {
            val need = length - builder.length
            if (need >= shuffledAlphabet.length) {
                builder.append(shuffledAlphabet)
            } else {
                builder.append(shuffledAlphabet.subSequence(0, need))
            }
        }
        assert(builder.length == length)
        return builder.toString()
    }
}