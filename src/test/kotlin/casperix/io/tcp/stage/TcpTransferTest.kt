package casperix.io.tcp.stage

import casperix.io.api.SocketBasedMessageStream
import casperix.io.codec.StringCodec
import casperix.io.tcp.stage.core.TcpStageCondition
import casperix.io.tcp.stage.core.TcpStageLauncher
import casperix.io.tcp.stage.core.TcpTestSupport.lockAddress
import casperix.io.tcp.stage.core.TcpTestSupport.receiveOrBlock
import kotlinx.coroutines.runBlocking
import java.util.concurrent.ConcurrentLinkedQueue
import java.util.concurrent.atomic.AtomicInteger
import kotlin.concurrent.thread
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class TcpTransferTest {

	@Test
	fun serverToClientMessage() {
		val receivedMessages = ConcurrentLinkedQueue<String>()
		val messageValue = "hello"

		TcpStageLauncher(1, StringCodec, lockAddress(), { },
			{
				it.channel.send(messageValue)
			}, {
				while (true) {
					receivedMessages += receiveOrBlock(it.channel)
				}
			}, waitingReasonList = listOf(
				{
					val received = receivedMessages.peek()
					if (received == null) {
						"message not received"
					} else if (received != messageValue) {
						"Expected $messageValue but actial $received"
					} else {
						null
					}
				},
				TcpStageCondition::noServerErrors,
				TcpStageCondition::noClientErrors,
				TcpStageCondition::isServerWorking,
				{ TcpStageCondition.isClientsWorking(it, 1) },
			)
		)
	}

	@Test
	fun clientToServerMessage() {
		val receivedMessages = ConcurrentLinkedQueue<String>()
		val messageValue = "hello"

		TcpStageLauncher(1, StringCodec, lockAddress(), {

		},
			{
				receivedMessages += receiveOrBlock(it.channel)
			}, {
				it.channel.send(messageValue)
			}, waitingReasonList = listOf(
				{
					val received = receivedMessages.peek()
					if (received == null) {
						"message not received"
					} else if (received != messageValue) {
						"Expected $messageValue but actial $received"
					} else {
						null
					}
				},
				TcpStageCondition::noServerErrors,
				TcpStageCondition::noClientErrors,
				TcpStageCondition::isServerWorking,
				{ TcpStageCondition.isClientsWorking(it, 1) },
			)
		)
	}

	data class MessageEntry(val output: Boolean, val isServer: Boolean, val channelIndex: Long, val message: String) {
		val input = !output
	}

	/**
	 * 	Checks for correct messaging when a large number of clients are running at the same time
	 */
	@Test
	fun fewClientsExchangeWithServer() {
		val summaryClientAmount = 100
		val messageFactory = listOf<(Boolean, Long) -> String>(
			{ isServer, channel -> "Hello from ${if (isServer) "server" else "client"}" },
			{ isServer, channel -> "My channel id is $channel" },
			{ isServer, channel -> "Last message" },
		)
		//	every channel has input & output message. every client servicing by two channel (server-side & client-side)
		val summaryMessageAmount = summaryClientAmount * messageFactory.size * 4
		val allMessages = ConcurrentLinkedQueue<MessageEntry>()
		val sendAmount = AtomicInteger(0)
		val receiveAmount = AtomicInteger(0)

		suspend fun channelService(isServer: Boolean, index: Long, channel: SocketBasedMessageStream<String>) {
			thread {
				runBlocking {
					messageFactory.forEach { factory ->
						val message = factory(isServer, index)
						allMessages.add(MessageEntry(true, isServer, index, message))
						channel.send(message)
						sendAmount.incrementAndGet()
					}

					messageFactory.forEach { _ ->
						val message = receiveOrBlock(channel)
						receiveAmount.incrementAndGet()
						allMessages.add(MessageEntry(false, isServer, index, message))
					}
				}
			}
		}

		TcpStageLauncher(
			summaryClientAmount, StringCodec, lockAddress(), { },
			{ (server, index, channel) -> channelService(true, index, channel) },
			{ (index, channel) -> channelService(false, index, channel) },
			waitingReasonList = listOf(
				{
					if (allMessages.size != summaryMessageAmount) "Expected $summaryMessageAmount messages but actual is ${allMessages.size}" else null
				},
				{
					if (sendAmount.get() != receiveAmount.get()) "Send ${sendAmount.get()} and receive ${receiveAmount.get()}" else null
				},
				TcpStageCondition::noServerErrors,
				TcpStageCondition::noClientErrors,
				TcpStageCondition::isServerWorking,
			),
			timeoutMs = 5_000L
		)

		checkMessages(allMessages.toList(), summaryClientAmount)

	}

	private fun checkMessages(messages: List<MessageEntry>, summaryClientAmount: Int) {
		(0L until summaryClientAmount).forEach { clientIndex ->
			//	because server & channel ID not equals...
			//	We use hack for define conformity
			val serverMessage = messages.filter { it.isServer && it.input && it.message == "My channel id is $clientIndex" }.firstOrNull()
			assertTrue(serverMessage != null)
			val serverIndex = serverMessage.channelIndex

			val serverOutput = messages.filter { it.output && it.channelIndex == serverIndex && it.isServer }
			val serverInput = messages.filter { it.input && it.channelIndex == serverIndex && it.isServer }

			val clientInput = messages.filter { it.input && it.channelIndex == clientIndex && !it.isServer }
			val clientOutput = messages.filter { it.output && it.channelIndex == clientIndex && !it.isServer }

			assertEquals(serverOutput.map { it.message }, clientInput.map { it.message })
			assertEquals(clientOutput.map { it.message }, serverInput.map { it.message })
		}

	}


}

