package casperix.io.tcp.stage.core

import casperix.io.api.SocketBasedMessageStream
import java.util.concurrent.ConcurrentLinkedQueue
import java.util.concurrent.atomic.AtomicReference

class TcpConnectionSlot<Message> {
	val channel = AtomicReference<SocketBasedMessageStream<Message>>()
	val errors = ConcurrentLinkedQueue<Throwable>()
}