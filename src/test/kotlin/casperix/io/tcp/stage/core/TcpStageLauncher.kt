package casperix.io.tcp.stage.core

import casperix.io.api.ChannelCodec
import casperix.io.tcp.SocketAccessBuilder
import casperix.io.tcp.TcpBuilder
import casperix.io.tcp.stage.core.TcpTestSupport.logger
import casperix.misc.DisposableHolder
import io.ktor.network.sockets.*
import kotlin.concurrent.thread

/**
 * 	Helper for create server and few clients.
 * 	Good for any tests
 */
class TcpStageLauncher<Message>(
    val clientAmount: Int,
    val codec: ChannelCodec<Message>,
    val address: InetSocketAddress,
    val onServer: (ServerSocket) -> Unit = {},
    val onServerChannel: suspend (TcpServerChannelEntry<Message>) -> Unit = {},
    val onClientChannel: suspend (TcpClientChannelEntry<Message>) -> Unit = {},
    val waitingReasonList: List<(TcpStage<Message>) -> String?>,
    timeoutMs: Long = 2000L,
) : DisposableHolder() {
    val stage = TcpStage<Message>(address, clientAmount)

    private val usedThreads = mutableListOf<Thread>()
    private var actualWaitingResult: List<String> = emptyList()


    private val launcherName = "[launcher:${address.port}]"

    init {
        logger.info("$launcherName starting...")
        createServerInThread()
        waitUntil(timeoutMs) {
            updateActualWaitingReason()
            actualWaitingResult.isEmpty()
        }
        dispose()
    }

    override fun dispose() {
        logger.info("dispose")
        usedThreads.forEach {
            it.interrupt()
        }
    }

    private fun updateActualWaitingReason() {
        actualWaitingResult = waitingReasonList.mapNotNull { waitingReason -> waitingReason(stage) }
    }

    private fun createServerInThread() {
        usedThreads += thread(name = "Server") {
            createServer()
        }
    }

    private fun createServer() {
        try {
            TcpBuilder.createServer(
                address, codec,
                {
                    stage.serverSocket.set(it)
                    onServer(it)
                    createClientsInThread()
                },
                { id, channel ->
                    logger.info("$launcherName server-channel $id start")
                    val slot = stage.serverChannelSlots[id.toInt()]
                    slot.channel.set(channel)
                    val serverSocket = stage.serverSocket.get()
                    val serverSocketAccess = SocketAccessBuilder.build(serverSocket)
                    onServerChannel(TcpServerChannelEntry(serverSocketAccess, id, channel))
                },
            ) {
                logger.error("$launcherName server error")
                stage.errors += it
            }
        } catch (_: InterruptedException) {
        } catch (e: Throwable) {
            logger.error("$launcherName server throwable:\n$e")
            throw e
        } finally {
            logger.info("$launcherName server closed")
        }
    }

    private fun createClientsInThread() {
        if (isDisposed) {
            throw Error("Launcher already disposed")
        }
        usedThreads += (0 until clientAmount).map { index ->
            thread(name = "Client $index") {
                Thread.sleep(index * 20L)
                createClient(index.toLong(), onClientChannel) {
                }
            }
        }
    }


    private fun createClient(
        index: Long,
        onClientChannel: suspend (TcpClientChannelEntry<Message>) -> Unit,
        readyEvent: () -> Unit
    ) {
        try {
            TcpBuilder.createClient(
                address,
                codec,
                {
                    readyEvent()
                    logger.info("$launcherName client $index start")
                    val slot = stage.clientChannelSlots[index.toInt()]
                    slot.channel.set(it)
                    onClientChannel(TcpClientChannelEntry(index, it))
                },
            ) {
                logger.error("$launcherName client $index error")
                val slot = stage.clientChannelSlots[index.toInt()]
                slot.errors += it
            }
        } catch (_: InterruptedException) {

        } catch (e: Throwable) {
            logger.error("$launcherName client $index throwable:\n$e")
            throw e
        } finally {
            logger.info("$launcherName client $index closed")
        }
    }

    private fun waitUntil(timeoutMs: Long = 1000L, condition: () -> Boolean) {
        TcpTestSupport.waitUntil(timeoutMs, condition, ::finish)
    }


    private fun finish() {
        if (actualWaitingResult.isNotEmpty()) {
            assert(false) { actualWaitingResult }
        } else {
            logger.info("Test passed successfully")
        }
    }
}