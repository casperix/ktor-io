package casperix.io.tcp.stage.core

import casperix.io.api.SocketBasedMessageStream
import casperix.io.codec.StringCodec
import casperix.io.tcp.TcpBuilder
import io.ktor.network.sockets.*
import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withTimeout
import org.apache.logging.log4j.LogManager
import java.util.concurrent.atomic.AtomicInteger

object TcpTestSupport {
	private val lastLockedPort = AtomicInteger(6000)
	val logger = LogManager.getLogger()

	fun lockAddress(): InetSocketAddress {
		val port = lastLockedPort.incrementAndGet()
		return InetSocketAddress("", port)
	}

	fun createEmptyClient(address: InetSocketAddress? = null) {
		TcpBuilder.createClient(address ?: lockAddress(), StringCodec, {

		})
	}

	fun createEmptyServer(address: InetSocketAddress? = null) {
		TcpBuilder.createServer(address ?: lockAddress(), StringCodec, { socket ->

		}, { id, channel ->

		})
	}

	suspend fun <Message> receiveOrBlock(it: SocketBasedMessageStream<Message>): Message {
		while (true) {
			val receivedData = it.receive()
			if (receivedData != null) return receivedData
			delay(10L)
		}
	}

	fun isErrorEquals(A: Throwable, B: Throwable): Boolean {
		return A.toString() == B.toString()
	}

	fun waitUntil(timeoutMs: Long = 1000L, condition: () -> Boolean, final: () -> Unit = {}) {
		runBlocking {
			try {
				withTimeout(timeoutMs) {
					while (!condition()) {
						delay(10L)
					}
				}
			} catch (e: Throwable) {
				logger.warn(e.localizedMessage)
			} finally {
				final()
			}
		}
	}

	fun waitTime(timeoutMs: Long = 2000L, block: () -> Unit) {
		runBlocking {
			withTimeout(timeoutMs) {
				block()
			}
		}
	}
}