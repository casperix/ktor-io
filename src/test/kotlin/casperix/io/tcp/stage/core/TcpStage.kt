package casperix.io.tcp.stage.core

import io.ktor.network.sockets.*
import java.util.concurrent.ConcurrentLinkedQueue
import java.util.concurrent.atomic.AtomicReference

class TcpStage<Message>(val address: InetSocketAddress, val clientAmount: Int) {
	val serverSocket = AtomicReference<ServerSocket>()
	val errors = ConcurrentLinkedQueue<Throwable>()
	val serverChannelSlots = Array(clientAmount) { index -> TcpConnectionSlot<Message>() }
	val clientChannelSlots = Array(clientAmount) { index -> TcpConnectionSlot<Message>() }
}