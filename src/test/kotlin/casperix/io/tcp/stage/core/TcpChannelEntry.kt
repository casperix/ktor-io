package casperix.io.tcp.stage.core

import casperix.io.api.SocketAccess
import casperix.io.api.SocketBasedMessageStream

interface TcpChannelEntry<Message> {
	val id: Long
	val channel: SocketBasedMessageStream<Message>
}

data class TcpClientChannelEntry<Message>(override val id: Long, override val channel: SocketBasedMessageStream<Message>) : TcpChannelEntry<Message>

data class TcpServerChannelEntry<Message>(val server: SocketAccess, override val id: Long, override val channel: SocketBasedMessageStream<Message>) : TcpChannelEntry<Message>