package casperix.io.tcp.stage.core

import io.ktor.network.sockets.*

object TcpStageCondition {

	fun <Message> findClientWithErrors(stage: TcpStage<Message>, expectedErrors: List<Throwable>): String? {
		if (stage.clientChannelSlots.isEmpty()) return "Expected client"

		val errors = stage.clientChannelSlots.map {
			checkErrors(expectedErrors, it.errors.toList())
		}
		if (errors.contains(null)) return null
		return errors.firstNotNullOf { it }
	}

	fun <Message> serverHasErrors(stage: TcpStage<Message>, expectedErrors: List<Throwable>): String? {
		return checkErrors(expectedErrors, stage.errors.toList())
	}


	private fun checkErrors(expectedErrors: List<Throwable>, actualErrors: List<Throwable>): String? {
		val expectedErrorNames = expectedErrors.map {
			it.localizedMessage
		}.sorted()

		val actualErrorNames = actualErrors.map {
			it.localizedMessage
		}.sorted()

		if (expectedErrorNames != actualErrorNames) return "Expected $expectedErrorNames but actual $actualErrorNames"
		return null
	}


	fun <Message> noServerErrors(stage: TcpStage<Message>): String? {
		val errors = stage.errors.map {
			it.localizedMessage
		}
		return if (errors.isNotEmpty()) "server errors: $errors" else null
	}


	fun <Message> noClientErrors(stage: TcpStage<Message>): String? {
		val errors = stage.clientChannelSlots.flatMap {
			it.errors.map {
				it.localizedMessage
			}
		}
		return if (errors.isNotEmpty()) "clients errors: $errors" else null
	}

	fun <Message> isServerWorking(stage: TcpStage<Message>): String? {
		val socket = stage.serverSocket.get()
		if (socket == null) return "server not created"
		if (socket.isClosed) return "server closed"
		return null
	}

	fun <Message> isServerClosed(stage: TcpStage<Message>): String? {
		val socket = stage.serverSocket.get()
		if (socket == null) return "server not created"
		if (!socket.isClosed) return "server working"
		return null
	}

	fun <Message> serverChannelAmount(stage: TcpStage<Message>, expectedAmount: Int): String? {
		val channelAmount = stage.serverChannelSlots.size
		return if (channelAmount != expectedAmount) "expected $expectedAmount server channel but actual $channelAmount" else null
	}

	fun <Message> isClientsWorking(stage: TcpStage<Message>, expectedAmount: Int): String? {
		val slotAmount = stage.clientChannelSlots.size
		if (slotAmount < expectedAmount) return "expected $expectedAmount client but has only $slotAmount slot"

		var workingClientAmount = 0

		(0 until slotAmount).forEach { index ->
			val clientSlot = stage.clientChannelSlots[index]
			val channel = clientSlot.channel.get()

			val isWorkingClient = channel != null && !channel.socket.isClosed
			if (isWorkingClient) workingClientAmount++
		}

		return if (workingClientAmount != expectedAmount) "expected $expectedAmount working client but actual $workingClientAmount" else null
	}
}