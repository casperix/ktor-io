package casperix.io.tcp

import casperix.io.api.SocketAccess
import io.ktor.network.sockets.*

object SocketAccessBuilder {
	fun build(socket: Socket): SocketAccess {
		return object : SocketAccess {
			override val address: String = socket.localAddress.toString()

			override fun close() {
				return socket.dispose()
			}

			override val isClosed get() = socket.isClosed
		}
	}

	fun build(socket: ServerSocket): SocketAccess {
		return object : SocketAccess {
			override val address: String = socket.localAddress.toString()

			override fun close() {
				return socket.dispose()
			}

			override val isClosed get() = socket.isClosed
		}
	}
}