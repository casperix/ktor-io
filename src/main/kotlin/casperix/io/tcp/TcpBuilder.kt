package casperix.io.tcp

import casperix.io.api.ChannelCodec
import casperix.io.api.SocketBasedMessageStream
import io.ktor.network.selector.*
import io.ktor.network.sockets.*
import kotlinx.coroutines.CancellationException
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.channels.ClosedReceiveChannelException
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import java.net.BindException
import java.nio.channels.ClosedChannelException
import java.util.concurrent.atomic.AtomicLong
import kotlin.coroutines.CoroutineContext

object TcpBuilder {
    private val customCoroutineContext = Dispatchers.IO//.limitedParallelism(10_000)

    fun defaultExceptionHandler(t: Throwable) {
        if (t !is CancellationException && t !is ClosedChannelException && t !is ClosedReceiveChannelException) {
            t.printStackTrace()
        }
    }

    /**
     * 	 Connecting a client to a given address.
     * 	The thread is blocked until the connection is closed
     */
    fun <Message> createClient(
        address: SocketAddress, codec: ChannelCodec<Message>,
        onConnection: suspend (SocketBasedMessageStream<Message>) -> Unit,
        onChannelFailure: (Throwable) -> Unit = TcpBuilder::defaultExceptionHandler,
    ) {
        try {
            runBlocking {
                socketBuilder(customCoroutineContext).connect(address).use { clientSocket ->
                    TcpConnection(clientSocket, codec, onConnection).servicing(onChannelFailure)
                }
            }
        } catch (_: CancellationException) {
            //	Service normally closed... Cancelled for example
        } catch (e: Throwable) {
            throw e
        }
    }

    /**
     * 	 Create a server at a given address or exception.
     * 	The thread is blocked until the server closed
     */
    fun <Message> createServer(
        address: SocketAddress,
        codec: ChannelCodec<Message>,
        onServer: suspend (ServerSocket) -> Unit,
        onServerChannel: suspend (Long, SocketBasedMessageStream<Message>) -> Unit,
        onChannelFailure: (Throwable) -> Unit = TcpBuilder::defaultExceptionHandler
    ) {
        try {
            val clientIdProvider = AtomicLong(0)
            runBlocking {
                socketBuilder(customCoroutineContext).bind(address) {
                    backlogSize = 8192
                }.use { serverSocket ->
                    onServer(serverSocket)

                    while (!serverSocket.isClosed) {
                        val clientSocket = serverSocket.accept()
                        launch {
                            TcpConnection(
                                clientSocket,
                                codec,
                                { onServerChannel(clientIdProvider.getAndIncrement(), it) }).servicing(onChannelFailure)
                        }
                    }
                }
            }
        } catch (exception: BindException) {
            println("Address already in use: $address")
            throw exception
        } catch (_: ClosedChannelException) {
            //	Service closed...
        } catch (_: CancellationException) {
            //	Service cancelled...
        } catch (e: Throwable) {
            throw e
        }
    }

    private fun socketBuilder(context: CoroutineContext): TcpSocketBuilder {
        val selector = SelectorManager(context)
        val builder = aSocket(selector)
        return builder.tcp()
    }
}

