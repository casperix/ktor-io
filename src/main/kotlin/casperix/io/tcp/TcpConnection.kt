package casperix.io.tcp

import casperix.io.SocketMessageChannelBuilder
import casperix.io.api.ChannelCodec
import casperix.io.api.SocketBasedMessageStream
import io.ktor.network.sockets.*
import kotlinx.coroutines.*
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.channels.Channel.Factory.UNLIMITED
import kotlinx.coroutines.channels.ClosedReceiveChannelException
import kotlinx.coroutines.channels.ClosedSendChannelException
import java.nio.channels.ClosedChannelException
import java.util.concurrent.atomic.AtomicInteger

@OptIn(DelicateCoroutinesApi::class)
class TcpConnection<Message>(val clientSocket: Socket, val codec: ChannelCodec<Message>, val onConnection: suspend (SocketBasedMessageStream<Message>) -> Unit) {
	val id = uniqueId.incrementAndGet()

	companion object {
		private val uniqueId = AtomicInteger(0)
	}

	private suspend fun unsafeServicing() = coroutineScope {
		val outputMessages = Channel<Message>(UNLIMITED)
		val outputBytes = clientSocket.openWriteChannel()

		val inputMessages = Channel<Message>(UNLIMITED)
		val inputBytes = clientSocket.openReadChannel()

		val channel = SocketMessageChannelBuilder(clientSocket, inputMessages, outputMessages).build()

		launch {
			while (!outputMessages.isClosedForReceive) {
				val message = outputMessages.receive()
				codec.encode(message, outputBytes)
				outputBytes.flush()
			}
		}

		launch {
			while (!inputMessages.isClosedForSend) {
				val message = codec.decode(inputBytes)
				inputMessages.send(message)
			}
		}

		launch {
			onConnection(channel)
		}
	}

	suspend fun servicing(onServiceFailure: (Throwable) -> Unit = TcpBuilder::defaultExceptionHandler) {
		try {
			unsafeServicing()
		} catch (_: ClosedChannelException) {
		} catch (_: ClosedSendChannelException) {
		} catch (_: ClosedReceiveChannelException) {
		} catch (e: CancellationException) {
			throw e
		} catch (cause: Throwable) {
			onServiceFailure(cause)
		} finally {
			closeSocket()
		}
	}

	private fun closeSocket() {
		clientSocket.dispose()
	}
}