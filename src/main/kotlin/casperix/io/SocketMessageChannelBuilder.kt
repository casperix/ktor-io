package casperix.io

import casperix.io.api.SocketAccess
import casperix.io.api.SocketBasedMessageStream
import casperix.io.tcp.SocketAccessBuilder
import io.ktor.network.sockets.*
import kotlinx.coroutines.channels.*

class SocketMessageChannelBuilder<Message>(
	val socket: Socket,
	val input: ReceiveChannel<Message>,
	val output: SendChannel<Message>,
) {


	fun build(): SocketBasedMessageStream<Message> {
		val socketAccess = SocketAccessBuilder.build(socket)
		return object : SocketBasedMessageStream<Message> {
			override val socket = socketAccess


			override fun send(message: Message) {
				val result = output.trySend(message)
				result.onSuccess {
					return
				}
				result.onFailure {
					if (it != null) {
						throw it
					} else {
						throw Error("Send failure (may be capacity limit reached)")
					}
				}
				result.onClosed {
					throw ClosedSendChannelException("")
				}
			}

			override fun receive(): Message? {
				val result = input.tryReceive()
				result.onSuccess {
					return it
				}
				result.onFailure {
					if (it != null) {
						throw it
					}
				}
				result.onClosed {
					throw ClosedReceiveChannelException("")
				}
				return null
			}

			override suspend fun receiving(): Message {
				return input.receive()
			}

			override suspend fun sending(message: Message) {
				output.send(message)
			}
		}
	}
}