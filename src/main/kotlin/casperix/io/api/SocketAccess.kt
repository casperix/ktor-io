package casperix.io.api

/**
 * 	API for message exchange and channel control
 */
interface SocketAccess {
	val address: String
	val isClosed: Boolean
	fun close()
}