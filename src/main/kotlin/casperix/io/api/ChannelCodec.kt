package casperix.io.api

import io.ktor.utils.io.*

/**
 * 	Decode abstract byte-channel to you custom message
 * 	Encode you custom message to byte-channel
 *
 * 	You can write self codec or use exist
 */
interface ChannelCodec<Message> {
	suspend fun decode(channel: ByteReadChannel): Message
	suspend fun encode(message: Message, channel: ByteWriteChannel)
}

