package casperix.io.api

interface SocketBasedMessageStream<Message> : MessageStream<Message> {
	val socket:SocketAccess
}

