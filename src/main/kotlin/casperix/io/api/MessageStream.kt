package casperix.io.api

/**
 * 	API for message exchange
 * 	Synchronous sending and receiving is available.
 * 	Suspend until result is also available
 */
interface MessageStream<Message> {
	/** Send message now or generate throw if can't	 */
	fun send(message: Message)

	/** Receive last message or null if queue is empty. Or generate throw if channel failed	 */
	fun receive(): Message?

	/**	Waiting until send success. Or throw if can't	*/
	suspend fun sending(message: Message)

	/**	Waiting until receive success. Or throw if can't	*/
	suspend fun receiving(): Message
}

