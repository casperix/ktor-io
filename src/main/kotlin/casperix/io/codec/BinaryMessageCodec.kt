package casperix.io.codec

import casperix.io.api.ChannelCodec
import io.ktor.utils.io.*

object BinaryMessageCodec : ChannelCodec<BinaryMessage> {
	override suspend fun decode(channel: ByteReadChannel): BinaryMessage {
		val type = channel.readInt()
		val size = channel.readInt()
		val bytes = ByteArray(size)
		channel.readFully(bytes)
		return BinaryMessage(type, size, bytes)
	}


	override suspend fun encode(message: BinaryMessage, channel: ByteWriteChannel) {
		channel.writeInt(message.type)
		channel.writeInt(message.data.size)
		channel.writeFully(message.data)
	}
}