package casperix.io.codec

import casperix.io.api.ChannelCodec
import io.ktor.utils.io.*
import kotlinx.coroutines.channels.ClosedReceiveChannelException
import java.nio.charset.CharsetEncoder

object StringCodec : ChannelCodec<String> {
	val charset = Charsets.UTF_8

	override suspend fun decode(channel: ByteReadChannel): String {
		val bytesAmount = channel.readInt()
		val bytes = ByteArray(bytesAmount)
		channel.readFully(bytes)
		return bytes.toString(charset)
	}

	override suspend fun encode(message: String, channel: ByteWriteChannel) {
		val bytes = message.toByteArray(charset)
		channel.writeInt(bytes.size)
		channel.writeFully(bytes)
	}

}