package casperix.io.codec

data class BinaryMessage(val type: Int, val length: Int, val data: ByteArray) {
	constructor(type: Int, bytes: ByteArray) : this(type, bytes.size, bytes)

	override fun equals(other: Any?): Boolean {
		if (this === other) return true
		if (javaClass != other?.javaClass) return false

		other as BinaryMessage

		if (type != other.type) return false
		if (length != other.length) return false
		if (!data.contentEquals(other.data)) return false

		return true
	}

	override fun hashCode(): Int {
		var result = type
		result = 31 * result + length
		result = 31 * result + data.contentHashCode()
		return result
	}


}